@extends('backend/layouts/master')
@section('title','Edit Blog')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="card-body">
                <h4 class="ml-2">Edit blog</h4>
                <hr>
                <form action="{{route('blog.update',$blog->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group col-4">
                        <label for="active">Active?</label>
                        <select class="form-control" name="status"
                        id="active">
                          <option value="1">Active</option>
                          <option value="0">Not-Active</option>
                        </select>
                      </div>
                    <div class="form-group form-float col-12">

                        <div class="form-line">
                            <label for="name">Title</label>
                        <input type="text" id="name" name="title" class="form-control" required value="{{$blog->title}}">
                        </div>
                    </div>
                    <div class="form-group form-float col-12">
                        <div class="form-line">
                            <label for="Description">Body</label>
                            <textarea name="body" id="body" cols="143"  rows="3">
                                {{$blog->body}}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group form-float col-12">
                         <a href="{{route('blog.index')}}" class="btn btn-danger m-t-15 waves-effect">
                        <i class="fas fa-backward mr-1"></i>
                            <span>Back</span>
                        </a>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                        <i class="fas fa-plus mr-1"></i>
                        <span>Update</span>
                    </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

@endpush


