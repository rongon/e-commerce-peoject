@extends('backend/layouts/master')
@section('title','EDIT CATEGORY')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="card-body">
                <h4 class="ml-2">ADD NEW CATEGORY</h4>
                <hr>
                <form action="{{route('categori.update',$category->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group col-4">
                        <label for="active">Active?</label>
                        <select class="form-control" name="status"
                        id="active">
                          <option value="1">Active</option>
                          <option value="0">Not-Active</option>
                        </select>
                      </div>
                    <div class="form-group form-float col-12">

                        <div class="form-line">
                            <label for="name">Title</label>
                        <input type="text" id="name" name="title" class="form-control" required value="{{$category->title}}">
                        </div>
                    </div>
                    <div class="form-group form-float col-12">
                        <div class="form-line">
                            <label for="Description">Description</label>
                            <textarea name="description" id="Description" cols="143"  rows="3">
                                {{$category->description}}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group form-float col-12">
                         <a href="{{route('categori.index')}}" class="btn btn-danger m-t-15 waves-effect">
                        <i class="fas fa-backward mr-1"></i>
                            <span>BACK</span>
                        </a>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                        <i class="fas fa-plus mr-1"></i>
                        <span>UPDATE</span>
                    </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

@endpush


