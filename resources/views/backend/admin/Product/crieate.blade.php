@extends('backend/layouts/master')
@section('title','ADD Product')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="card-body">
                <h4 class="ml-2">ADD NEW product</h4>
                <hr>
                <form action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-4">
                        <label for="active">Active?</label>
                        <select class="form-control" name="status"
                        id="active">
                          <option value="1">Active</option>
                          <option value="0">Not-Active</option>
                        </select>
                      </div>
                    <div class="form-group col-4">
                        <label for="active">Categiry</label>
                        <select class="form-control" name="categori_id">
                            @foreach ($categoris as $categori)
                                <option value="{{$categori->id}}">{{$categori->title}}</option>
                            @endforeach

                        </select>
                      </div>
                    <div class="form-group form-float col-12">
                        <div class="form-line">
                            <label for="name">Title</label>
                            <input type="text" id="name" name="title" class="form-control" required placeholder="product Title">
                        </div>
                    </div>
                    <div class="form-group form-float col-12">
                        <div class="form-line">
                            <label for="Description">Description</label>
                            <textarea name="description" id="Description" cols="143"  rows="3">
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group form-float col-12">
                        <div class="form-line">
                            <label for="image">Image</label><br>
                            <input type="file" id="image" name="image" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-float col-12">
                        <div class="form-line">
                            <label for="Description">Color</label>&nbsp;&nbsp;&nbsp;
                            @foreach ($colors as $color)
                        <input type="checkbox" name="color[]" value="{{$color->id}}" > {{$color->name}}
                            @endforeach

                        </div>
                    </div>

                    <div class="form-group form-float col-12">
                         <a href="{{route('product.index')}}" class="btn btn-danger m-t-15 waves-effect">
                        <i class="fas fa-backward mr-1"></i>
                            <span>BACK</span>
                        </a>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                        <i class="fas fa-plus mr-1"></i>
                        <span>ADD</span>
                    </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    {{-- <script src="{{asset('ui/backend')}}/libs/footable/footable.all.min.js"></script>

<!-- Init js -->
<script src="{{asset('ui/backend')}}/js/pages/foo-tables.init.js"></script> --}}
@endpush


