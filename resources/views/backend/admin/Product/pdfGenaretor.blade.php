
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title">product-List</h4>
           <hr>

            <div class="">
                <table id="demo-foo-filtering" class="table toggle-circle mb-0 table-striped " data-page-size="7">
                    <tr>
                        <th >Id</th>
                        <th>Title</th>
                        <th >Description</th>
                        <th >Status</th>
                    </tr>
                        @foreach ($products as $key=>$product)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$product->title}}</td>
                        <td>{{$product->description}}</td>
                        <td>
                            @if ($product->status == 1)
                                {{ "Active" }}
                            @else
                                {{"Not-Active"}}
                            @endif
                        </td>
                    </tr>
                    @endforeach

                </table>

            </div> <!-- end .table-responsive-->
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>



