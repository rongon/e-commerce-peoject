@extends('backend/layouts/master')
@section('title','Product')
@section('content')
<div class="row">
    <div class=" col-12">
         <div class="card">
        <div class="card-body">
            <table id="demo-foo-filtering" class="table toggle-circle mb-0 table-striped " data-page-size="7">
                <tr>
                    <th >Title</th>
                    <td>{{$show->title}}</th>
                </tr>
                <tr>
                    <th >Description</th>
                    <td>{{$show->description}}</th>
                </tr>
                <tr>
                    <th >Products</th>
                    <td>
                        <ul>
                            @foreach ($show->colors as $product)
                                <li>{{$product->name}}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                <tr> @if (count($show->comments)>0)
<th >Comments</th>
                    <td>
                        <ol>
                            @foreach ($show->comments as $comment)
                        <li>
                            <b>{{$comment->body}}->{{$comment->commentedBy->name}}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>

                            <mark>{{$comment->created_at->toFormattedDateString()}}</mark>
                            {{$comment->created_at->diffForHumans()}}

                        </li>
                            @endforeach
                        </ol>
                    </td>
                @endif

                </tr>
                <tr>
                    <th >Comment</th>
                    <td>
                        <form action="{{route('product/comment',$show->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group form-float col-12">
                                <div class="form-line">
                                    <textarea name="body" id="body" cols="130"  rows="3">
                                    </textarea>
                                </div>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                                    <i class="fas fa-plus mr-1"></i>
                                    <span>Comment</span>
                                </button>
                            </div>
                        </form>

                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>

</div>
@endsection

@push('js')
    {{-- <script src="{{asset('ui/backend')}}/libs/footable/footable.all.min.js"></script>

<!-- Init js -->
<script src="{{asset('ui/backend')}}/js/pages/foo-tables.init.js"></script> --}}
@endpush


