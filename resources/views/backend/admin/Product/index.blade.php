@extends('backend/layouts/master')
@section('title','product-list')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title">product-List</h4>
           <hr>
            <div class="mb-2">
                <div class="row">
                    <div class="col-8 text-sm-center form-inline">
                        <a href="{{route('product.pdf')}}" type="button" class="btn btn-secondary mr-1">
                            <i class="fas fa-download"></i>
                            <span>PDF</span>
                        </a>
                        <a href="{{route('product.exel')}}" type="button" class="btn btn-secondary">
                            <i class="fas fa-download"></i>
                            <span>EXEL</span>
                        </a>
                    </div>
                    <div class=" col-4 ">
                        <a href="{{route('product.create')}}" type="button" class="btn btn-outline-info">
                            <i class="fas fa-plus"></i>
                            <span>ADD NEW PRODUCT</span>
                        </a>
                        {{-- <a href="{{route('product.trashlist')}}" type="button" class="btn btn-outline-danger">
                            <i class="fas fa-trash"></i>
                            <span>TRASH LIST</span>
                        </a> --}}
                    </div>
                </div>
            </div>

            <div class="">
                <table id="demo-foo-filtering" class="table toggle-circle mb-0 table-striped " data-page-size="7">
                    <tr>
                        <th >Id</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th >Description</th>
                        <th >Status</th>
                        <th >createdBy</th>
                        <th >Action</th>
                    </tr>
                        @foreach ($products as $key=>$product)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$product->title}}</td>
                        <td>{{$product->categori->title}}</td>
                        <td>{{ Str::limit($product->description, 50) }}</td>
                        {{-- {{$product->description}} --}}
                        <td>
                            @if ($product->status == 1)
                                {{ "Active" }}
                            @else
                                {{"Not-Active"}}
                            @endif
                        </td>
                        <td>{{$product->user->name??NULL}}</td>
                        <td class="d-flex">
                            <span>
                            <a href="{{route('product.show',$product->id)}}" type="button" class="btn btn-outline-primary mr-1">
                                <i class="fas fa-eye"></i>
                                </a>
                            </span>
                            <span>
                                <a href="{{route('product.edit',$product->id)}}" type="button" class="btn btn-outline-success mr-1 ">
                                <i class="fas fa-edit"></i>
                                </a>
                            </span>
                            <span>
                                <form action="{{route('product.destroy',$product->id)}}" method="post">
                                    @csrf
                                    @method('Delete')
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </form>
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </table>

            </div> <!-- end .table-responsive-->
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
@endsection

@push('js')
  {{-- <script src="{{asset('ui/backend')}}/libs/footable/footable.all.min.js"></script> --}}

<!-- Init js -->
<script src="{{asset('ui/backend')}}/js/pages/foo-tables.init.js"></script>
@endpush


