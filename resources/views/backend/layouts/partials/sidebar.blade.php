<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Navigation</li>

                <li>
                    <a href="{{route('admin/dashboard')}}" class="waves-effect">
                        <i class="remixicon-dashboard-line"></i>
                        <span> Dashboards </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('categori.index')}}" class="waves-effect">
                        <i class="fas fa-list-ul"></i>
                        <span> Categori </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('product.index')}}" class="waves-effect">
                        <i class="fas fa-list-ul"></i>
                        <span> Product </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('blog.index')}}" class="waves-effect">
                        <i class="fas fa-list-ul"></i>
                        <span> Blog </span>
                    </a>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
