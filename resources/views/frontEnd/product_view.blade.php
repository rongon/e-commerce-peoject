@extends('frontEnd.layouts.master')

@section('title','Blog')

@section('css')

<link href="{{asset('ui/frontEnd/css/flexslider.css')}}" type="text/css" rel="stylesheet" media="all">

@endsection

@section('pageContent')

@include('frontEnd.layouts.partials.ibanner')
@include('frontEnd.layouts.partials.breadcrump')

@include('frontEnd.layouts.elements.product_view.details')
@include('frontEnd.layouts.elements.product_view.similer')


@endsection

@section('script')
<script src="{{asset('ui/frontEnd/js/jquery.flexslider.js')}}"></script>
<script src="{{asset('ui/frontEnd/js/imagezoom.js')}}"></script>
<script>
    // Can also be used with $(document).ready()
    $(window).load(function () {
        $('.flexslider1').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>
<!-- //FlexSlider-->
@endsection

