<div class="row checkout-left mt-5">
    <div class="col-md-4 checkout-left-basket">
        <h4>Continue to basket</h4>
        <ul>
            <li>Solid Men's Black Shirt
                <i>-</i>
                <span>$20.00 </span>
            </li>
            <li>Women's Light Blue Tunic
                <i>-</i>
                <span>$35.00 </span>
            </li>
            <li>Boy's Casual Shirt & Trouser Set
                <i>-</i>
                <span>$23.00</span>
            </li>
            <li>Total
                <i>-</i>
                <span>$78.00</span>
            </li>
        </ul>
    </div>
    <div class="col-md-8 address_form">
        <h4>Billing Address</h4>
        <form action="payment.html" method="post" class="creditly-card-form shopf-sear-headinfo_form">
            <div class="creditly-wrapper wrapper">
                <div class="information-wrapper">
                    <div class="first-row form-group">
                        <div class="controls">
                            <label class="control-label">Full name: </label>
                            <input class="billing-address-name form-control" type="text" name="name"
                                placeholder="Full name">
                        </div>
                        <div class="card_number_grids">
                            <div class="card_number_grid_left">
                                <div class="controls">
                                    <label class="control-label">Mobile number:</label>
                                    <input class="form-control" type="text" placeholder="Mobile number">
                                </div>
                            </div>
                            <div class="card_number_grid_right">
                                <div class="controls">
                                    <label class="control-label">Landmark: </label>
                                    <input class="form-control" type="text" placeholder="Landmark">
                                </div>
                            </div>
                            <div class="clear"> </div>
                        </div>
                        <div class="controls">
                            <label class="control-label">Town/City: </label>
                            <input class="form-control" type="text" placeholder="Town/City">
                        </div>
                        <div class="controls">
                            <label class="control-label">Address type: </label>
                            <select class="form-control option-fieldf">
                                <option>Office</option>
                                <option>Home</option>
                                <option>Commercial</option>

                            </select>
                        </div>
                    </div>
                    <button class="submit check_out">place order</button>
                </div>
            </div>
        </form>
    </div>
</div>
