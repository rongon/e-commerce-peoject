<div class="d-sm-flex justify-content-between mb-4">
    <h4>review your order
    </h4>
    <h4 class="mt-sm-0 mt-3">Your shopping cart contains:
        <span>3 Products</span>
    </h4>
</div>
<div class="checkout-right">
    <table class="timetable_sub">
        <thead>
            <tr>
                <th>SL No.</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Product Name</th>

                <th>Price</th>
                <th>Remove</th>
            </tr>
        </thead>
        <tbody>
            <tr class="rem1">
                <td class="invert">1</td>
                <td class="invert-image">
                    <a href="single_product.html">
                        <img src="{{asset('ui/frontEnd/images/pm1.jpg')}}" alt=" " class="img-responsive">
                    </a>
                </td>
                <td class="invert">
                    <div class="quantity">
                        <div class="quantity-select">
                            <div class="entry value-minus">&nbsp;</div>
                            <div class="entry value">
                                <span>1</span>
                            </div>
                            <div class="entry value-plus active">&nbsp;</div>
                        </div>
                    </div>
                </td>
                <td class="invert">Solid Men's Black Shirt</td>

                <td class="invert">$20.00</td>
                <td class="invert">
                    <div class="rem">
                        <div class="close1"> </div>
                    </div>

                </td>
            </tr>
            <tr class="rem2">
                <td class="invert">2</td>
                <td class="invert-image">
                    <a href="single_product.html">
                        <img src="{{asset('ui/frontEnd/images/pf1.jpg')}}" alt=" " class="img-responsive">
                    </a>
                </td>
                <td class="invert">
                    <div class="quantity">
                        <div class="quantity-select">
                            <div class="entry value-minus">&nbsp;</div>
                            <div class="entry value">
                                <span>1</span>
                            </div>
                            <div class="entry value-plus active">&nbsp;</div>
                        </div>
                    </div>
                </td>
                <td class="invert">Women's Light Blue Tunic</td>

                <td class="invert">$35.00</td>
                <td class="invert">
                    <div class="rem">
                        <div class="close2"> </div>
                    </div>

                </td>
            </tr>
            <tr class="rem3">
                <td class="invert">3</td>
                <td class="invert-image">
                    <a href="single_product.html">
                        <img src="{{asset('ui/frontEnd/images/pb3.jpg')}}" alt=" " class="img-responsive">
                    </a>
                </td>
                <td class="invert">
                    <div class="quantity">
                        <div class="quantity-select">
                            <div class="entry value-minus">&nbsp;</div>
                            <div class="entry value">
                                <span>1</span>
                            </div>
                            <div class="entry value-plus active">&nbsp;</div>
                        </div>
                    </div>
                </td>
                <td class="invert">Boy's Casual Shirt & Trouser Set</td>

                <td class="invert">$23.00</td>
                <td class="invert">
                    <div class="rem">
                        <div class="close3"> </div>
                    </div>

                </td>
            </tr>

        </tbody>
    </table>
</div>
