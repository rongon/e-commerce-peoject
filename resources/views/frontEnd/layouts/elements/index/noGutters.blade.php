<div class="row no-gutters pb-5">
    <div class="col-sm-4">
        <div class="hovereffect">
            <img class="img-fluid" src="{{asset('ui/frontEnd/images')}}/a1.jpg" alt="">
            <div class="overlay">
                <h5>women's fashion</h5>
                <a class="info" href="women.html">Shop Now</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="hovereffect">
            <img class="img-fluid" src="{{asset('ui/frontEnd/images')}}/a2.jpg" alt="">
            <div class="overlay">
                <h5>men's fashion</h5>
                <a class="info" href="men.html">Shop Now</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="hovereffect">
            <img class="img-fluid" src="{{asset('ui/frontEnd/images')}}/a3.jpg" alt="">
            <div class="overlay">
                <h5>kid's fashion</h5>
                <a class="info" href="girls.html">Shop Now</a>
            </div>
        </div>
    </div>
</div>
