@extends('frontEnd.layouts.master')

@section('title','Home')

@section('pageContent')

    @include('frontEnd.layouts.elements.index.bannerText')
    @include('frontEnd.layouts.elements.index.agileitsServices')
    @include('frontEnd.layouts.elements.index.noGutters')
    @include('frontEnd.layouts.elements.index.smartClothing')
    @include('frontEnd.layouts.elements.index.shopOnInstra')

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $("#myModal_btn").modal();
    });
</script>
@endsection
