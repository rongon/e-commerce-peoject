@extends('frontEnd.layouts.master')

@section('title','Blog')

@section('pageContent')

@include('frontEnd.layouts.partials.ibanner')
@include('frontEnd.layouts.partials.breadcrump')


<div class="innerf-pages section">
    <div class="fh-container mx-auto">
        <div class="row my-lg-5 mb-5">
            <!-- grid left -->
            @include('frontEnd.layouts.elements.products.sidebar')
            <!-- //grid left -->
            <!-- grid right -->
            @include('frontEnd.layouts.elements.products.products_grid')
        </div>
    </div>
</div>


@endsection


