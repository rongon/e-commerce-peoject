@extends('frontEnd.layouts.master')

@section('title','Checkout')

@section('css')
<link rel="stylesheet" href="{{asset('ui/frontEnd/css/checkout.css')}}" type="text/css" media="all" />

@endsection

@section('pageContent')

@include('frontEnd.layouts.partials.ibanner')
@include('frontEnd.layouts.partials.breadcrump')

<section class="checkout_wthree py-sm-5 py-3">
    <div class="container">
        <div class="check_w3ls">

            @include('frontEnd.layouts.elements.checkout.reviewCheckout')
            @include('frontEnd.layouts.elements.checkout.billAddress')

        </div>
    </div>
</section>


@endsection

@section('script')
    <!--quantity-->
    <script>
        $('.value-plus').on('click', function () {
            var divUpd = $(this).parent().find('.value'),
                newVal = parseInt(divUpd.text(), 10) + 1;
            divUpd.text(newVal);
        });

        $('.value-minus').on('click', function () {
            var divUpd = $(this).parent().find('.value'),
                newVal = parseInt(divUpd.text(), 10) - 1;
            if (newVal >= 1) divUpd.text(newVal);
        });
    </script>
    <!--quantity-->
@endsection

