@extends('frontEnd.layouts.master')

@section('title','Home')

@section('pageContent')

    @include('frontEnd.layouts.partials.ibanner')
    @include('frontEnd.layouts.partials.breadcrump')
    @include('frontEnd.layouts.elements.index.agileitsServices')
    @include('frontEnd.layouts.elements.index.noGutters')
    @include('frontEnd.layouts.elements.index.smartClothing')
    @include('frontEnd.layouts.elements.index.shopOnInstra')

@endsection
