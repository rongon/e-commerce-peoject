<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categori extends Model
{

    use SoftDeletes;
    protected $fillable = ['status','title','description'];
    public function products(){
        return $this->hasMany(Product::class);
    }
}
