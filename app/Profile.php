<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['bio','facebook_url','twitter_url','linked-in_url','github_url'];
   public function user(){
       return $this->belongsTo(User::class);
   }
}
