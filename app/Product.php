<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    protected $fillable = ['categori_id','title','description','status','image','','created_by','updated_by'];
    public function categori(){
        return $this->belongsTo(Categori::class);
    }
    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }
    public function colors(){
        return $this->belongsToMany(Color::class);
    }

    public function tags(){
        return $this->morphToMany(Tag::class,'taggable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
