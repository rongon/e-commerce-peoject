<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\CategoriesExport;
use Maatwebsite\Excel\Facades\Excel;

class ExelController extends Controller
{
    public function category()
    {
        return Excel::download((new CategoriesExport), 'category.xlsx');
    }
}
