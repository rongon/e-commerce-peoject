<?php

namespace App\Http\Controllers;
use App\Categori;

use Illuminate\Http\Request;

class softDeleteController extends Controller
{
    public function trashList(){
        $trashs = Categori::onlyTrashed()->get();
        return view('backend.admin.categori.trashList',compact('trashs'));

    }
    public function trashRestore($id){
        Categori::withTrashed()->where('id', $id)->restore();
        return redirect()->back()->with('success','Data Restore successful');
    }
    public function trashDelete($id){
        $delete = Categori::onlyTrashed()->where('id', $id)->first();
        $delete->forceDelete();
        return redirect()->back()->with('success','Parmanent Delete successful');
    }

}
