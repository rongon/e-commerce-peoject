<?php

namespace App\Http\Controllers;

use App\Color;
use App\Product;
use App\Categori;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    //
    public function index()
    {
       $products = Product::latest()->with('user')->get();
        return view('backend.admin.Product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoris = Categori::latest()->get();
        $colors = Color::get();
        return view('backend.admin.Product.crieate',compact('categoris','colors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'image' => 'mimes:jpeg,png,jpg'
        ]);
        $image = $request->file('image');
        $slug = Str::slug($request->title);
        if(isset($image)){
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('product')){
                Storage::disk('public')->makeDirectory('product');
            }
            $path = $image->store('public/product');
        }




        $data = $request->all();
        $data['image'] = $imageName;
        $data['created_by'] = Auth::user()->id;
        $color = $data['color'];
        $product = Product::create($data);
        $product->colors()->attach($color);
        return redirect (route('product.index'))->with('success','insart successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = Product::find($id);

        return view('backend.admin.Product.show',compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categoris = Categori::latest()->get();
        $colors = Color::get();
        $selected_color = $product->colors->pluck('id')->toArray();
        return view('backend.admin.Product.edit',compact('product','categoris','colors','selected_color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $color = $data['color'];
        $update = Product::find($id);
        $update->update($data);
        $update->colors()->sync($color);
        return redirect (route('product.index'))->with('success','Updated successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Product::find($id);
        $color = $delete['color'];
        $delete->colors()->detach($color);
        $delete->delete();
        return redirect()->back()->with('danger','Moved To Trash-List');
    }
}
