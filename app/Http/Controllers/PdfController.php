<?php

namespace App\Http\Controllers;

use PDF;
use App\Categori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{
    public function category(){
       $categoris = Categori::latest()->get();
        $pdf = PDF::loadView('backend.admin.categori.pdfGenaretor',compact('categoris'));
        return $pdf->download('invoice.pdf');
    }
}
