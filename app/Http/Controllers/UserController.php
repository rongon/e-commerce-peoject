<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\FuncCall;

class UserController extends Controller
{
    public function myProfile(){
        $user = auth()->user();
        return view('backend.admin.user.profile',compact('user'));

    }
    public function updateProfile(Request $request){
        $user = auth()->user();
        $userUpdate = $request->only('name','email');
        $profileUpdate = $request->only('bio','facebook_url','twitter_url','linked-in_url','github_url');

        $user->update( $userUpdate);
        $user->profile->update( $profileUpdate);
        //return $user->profile;
        return redirect (route('my_profile'))->with('success','Updated successful');
    }
}
