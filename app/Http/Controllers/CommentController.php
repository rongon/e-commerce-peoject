<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function product(Request $request,Product $product){

        $product->comments()->create([
            'body' => $request->body,
            'commented_by' => auth()->user()->id
        ]);
            return redirect()->back();

    }
}
