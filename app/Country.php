<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name','created_by'];

    public function user(){
        return $this->hasMany(User::class);
    }
}
