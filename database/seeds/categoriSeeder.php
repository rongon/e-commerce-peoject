<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoris')->insert([
            'title'=> 'mens',
            'description'=> 'recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
            'is-active'=>'1'
        ]);
        DB::table('categoris')->insert([
            'title'=> 'womens',
            'description'=> 'recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
            'is-active'=>'1'
        ]);
        DB::table('categoris')->insert([
            'title'=> 't-shirt',
            'description'=> 'recently with desktop publishing software like Aldus PageMaker including ',
            'is-active'=>'1'
        ]);
    }
}
