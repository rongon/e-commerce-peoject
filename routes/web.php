<?php

use App\Http\Controllers\CommentController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontEnd.index');
});

Route::get('/faq', function () {
    return view('frontEnd.faq');
});

Route::get('/blog', function () {
    return view('frontEnd.blog');
});

Route::get('/payment', function () {
    return view('frontEnd.payment');
});

Route::get('/checkout', function () {
    return view('frontEnd.checkout');
});

Route::get('/contact', function () {
    return view('frontEnd.contact');
});

Route::get('/single', function () {
    return view('frontEnd.single');
});

Route::get('/shop', function () {
    return view('frontEnd.shop');
});

Route::get('/product_view', function () {
    return view('frontEnd.product_view');
});

Route::get('/products', function () {
    return view('frontEnd.products');
});

Route::get('/about', function () {
    return view('frontEnd.about');
});





Route::group(['prefix' => 'admin','middleware'=>'auth'], function () {

    Route::get('category/pdf', 'PdfController@category')->name('category.pdf');
    Route::get('category/exel', 'ExelController@category')->name('category.exel');

    Route::get('product/pdf', 'PdfController@category')->name('product.pdf');
    Route::get('product/exel', 'ExelController@category')->name('product.exel');

    Route::put('trashRestore/{id}' , 'softDeleteController@trashRestore')->name('cetagori.trashRestore');

    Route::delete('trashDelete/{id}' , 'softDeleteController@trashDelete')->name('cetagori.trashDelete');



    // Route::put('trashRestore/{id}' , 'softDeleteController@trashRestore')->name('product.trashRestore');

    // Route::delete('trashDelete/{id}' , 'softDeleteController@trashDelete')->name('product.trashDelete');

    Route::get('trashlist' , 'softDeleteController@trashList')->name('product.trashlist');

    Route::get('trashlist' , 'softDeleteController@trashList')->name('categori.trashlist');



    Route::resource('categori', 'CategoriController');
    Route::resource('product', 'ProductController');
    Route::resource('blog', 'BlogController');
    Route::get('color/test', 'ColorController@test');



    Route::get('dashboard', 'HomeController@index')->name('admin/dashboard');

    Route::get('my-profile', 'UserController@myProfile')->name('my_profile');
    Route::put('update-profile', 'UserController@updateProfile')->name('update_profile');

    Route::post('product/{product}/comment', 'CommentController@product')->name('product/comment') ;

});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

